﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Интерфейс для патерна репозиторий
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T: BaseEntity
    {
        /// <summary>
        /// Получить список
        /// </summary>
        /// <returns></returns>
        Task<IList<T>> GetAllAsync();
        
        /// <summary>
        /// Поиск по ID
        /// </summary>
        /// <param name="id">ID для поиска</param>
        /// <returns></returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Создание нового объекта
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
         Task<Guid> CreateAsync(T Entity);

    }
}