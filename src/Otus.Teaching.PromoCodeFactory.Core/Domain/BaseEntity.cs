﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    /// <summary>
    /// Базовая модель
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public BaseEntity()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Идентификатор
        /// </summary>
        [Required, Key]
        public Guid Id { get; set; }
    }
}