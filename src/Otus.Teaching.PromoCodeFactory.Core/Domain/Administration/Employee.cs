﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class Employee : BaseEntity
    {
        /// <summary>
        /// Имя 
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Полное имя
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Роли
        /// </summary>
        public virtual List<Role> Roles { get; set; }

        /// <summary>
        /// Применные промокоды
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}