﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    /// <summary>
    /// Роль 
    /// </summary>
    public class Role: BaseEntity
    {
        /// <summary>
        /// Наименование роли
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Описание роли
        /// </summary>
        public string Description { get; set; }
    }
}